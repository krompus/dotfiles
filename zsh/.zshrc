#    _                                                    _              
#   | |                                                  | |             
#   | | ___ __ ___  _ __ ___  _ __  _   _ ___     _______| |__  _ __ ___ 
#   | |/ / '__/ _ \| '_ ` _ \| '_ \| | | / __|   |_  / __| '_ \| '__/ __|
#   |   <| | | (_) | | | | | | |_) | |_| \__ \  _ / /\__ \ | | | | | (__ 
#   |_|\_\_|  \___/|_| |_| |_| .__/ \__,_|___/ (_)___|___/_| |_|_|  \___|
#                            | |                                         
#                            |_|                                         
#  
#
#                   ~~ beware all ye who enter; here be weird binds! ~~
 
plugins=(git zsh-autosuggestions)

# User configuration

# tmux on every shell, kill term when tmux is exited
#if [ "$TMUX" = "" ]; then tmux && exit; fi
#
# TMUX
#if which tmux >/dev/null 2>&1; then
#    # if no session is started, start a new session
#    test -z ${TMUX} && tmux && exit
#fi
#
## tmux on every shell, drop to shell when tmux is exited
#if [ "$TMUX" = "" ]; then tmux; fi

export PATH="/home/krompus/bin:/usr/local/bin:/home/krompus/GNUstep/Tools:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/krompus/bin:/home/krompus/.fzf/bin:/home/krompus/bin"
# export MANPATH="/usr/local/man:$MANPATH"

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

export WORDCHARS='*?_[]~=&;!#$%^(){}'

zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

#source $ZSH/oh-my-zsh.sh
source ~/.zsh/key-bindings.zsh

bindkey -e

stty -ixon
setopt append_history
setopt inc_append_history
setopt noflowcontrol
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_save_no_dups

export BROWSER="firefox"
export EDITOR="vim"
export RANGER_LOAD_DEFAULT_RC FALSE
#export FZF_COMPLETION_TRIGGER=''
#export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
#export FZF_DEFAULT_OPTS="--reverse --inline-info"
#bindkey '^T' fzf-completion
bindkey "${terminfo[kcbt]}" reverse-menu-complete
#PATH=$PATH:~/bin
#export PATH

#source ~/.zsh.sh
source ~/.alias

#zle -N copyx; copyx() { echo -E - $BUFFER | xclip -i }; bindkey '^K' copyx
zle -N copyx; copyx() { echo -E $BUFFER | xsel -ib }; bindkey '^X' copyx
alias curl='noglob curl'
alias youtube-dl='noglob youtube-dl'
alias ytdlp='noglob ytdlp'
alias ytdlm='noglob ytdlm'
alias ytdl='noglob ytdl'

FZF_DEFAULT_OPTS='
  --color hl:177,fg+:12,hl+:207
  --color info:36,spinner:107,pointer:197,marker:149
'

# Suffix aliases; Open with
alias -s jpg='sxiv-rifle'
alias -s jpeg='sxiv-rifle'
alias -s png='sxiv-rifle'
alias -s gif='mpv --loop=8'
alias -s webm='mpv --loop=8'
alias -s avi='mpv'
alias -s flv='mpv'
alias -s mkv='mpv'
alias -s mp3='mpv'
alias -s mp4='mpv'
alias -s ogg='mpv'

alias \rm='rm -I --preserve-root'

autoload -Uz compinit
compinit
compdef vman="man"
autoload -Uz zmv
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.zhist
HISTSIZE=10000
SAVEHIST=10000

# -*- mode: shell-script -*-
# vim: set ft=zsh :
alias priv=' PRIV=1 zsh'

if [ -n "$PRIV" ]; then
    unset HISTFILE
    fc -R $DEF_HISTFILE
    bindkey -s '\er' "^u ranger -c\n"
fi
eval "$(thefuck --alias)"
eval "$(dircolors ~/.dircolors)"

autoload -U colors zsh/terminfo
colors

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git hg
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*' formats "%{${fg[cyan]}%}[%{${fg[green]}%}%s%{${fg[cyan]}%}][%{${fg[blue]}%}%r/%S%%{${fg[cyan]}%}][%{${fg[blue]}%}%b%{${fg[yellow]}%}%m%u%c%{${fg[cyan]}%}]%{$reset_color%}"
#zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
#zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
#zstyle ':completion:*' completer _complete _ignored
#zstyle :compinstall filename '/home/krompus/.zshrc'

setprompt() {
  # load some modules
  setopt prompt_subst


  # make some aliases for the colours: (coud use normal escap.seq's too)
  for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
    eval PR_$color='%{$fg[${(L)color}]%}'
  done
  PR_NO_COLOR="%{$terminfo[sgr0]%}"

  # Check the UID
  if [[ $UID -ge 1000 ]]; then # normal user
    eval PR_USER='%B${PR_BLUE}%B%n${PR_NO_COLOR}'
    eval PR_USER_OP='${PR_BLUE}%#${PR_NO_COLOR}'
  elif [[ $UID -eq 0 ]]; then # root
    eval PR_USER='${PR_RED}%B%n${PR_NO_COLOR}'
    eval PR_USER_OP='${PR_RED}%#${PR_NO_COLOR}'
  fi

  # Check if we are on SSH or not
  if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then 
    eval PR_HOST='${PR_YELLOW}%B%M${PR_NO_COLOR}' #SSH
  else 
    eval PR_HOST='${PR_GREEN}%B%M${PR_NO_COLOR}' # no SSH
  fi

  # set the prompt
  PS1=$'${PR_USER}%B${PR_WHITE}@${PR_HOST} ${PR_RED}%B%~/ ${PR_USER_OP} '
  PS2=$'%_>'
  RPROMPT=$'${vcs_info_msg_0_}'
}
setprompt

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
